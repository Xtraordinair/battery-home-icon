.class Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;
.super Landroid/content/BroadcastReceiver;
.source "XposedMod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->registerReceiverIfNeeded(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;


# direct methods
.method constructor <init>(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)V
    .locals 0
    .param p1, "this$0"    # Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 224
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 225
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 226
    const-string v1, "level"

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    const-string v2, "scale"

    const/16 v3, 0x64

    .line 227
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    div-int v0, v1, v2

    .line 228
    .local v0, "level":I
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setBatteryLevel(I)V

    .line 230
    const-string v1, "plugged"

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_1

    .line 231
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setBatteryCharging(Z)V

    .line 236
    :goto_0
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    iget-object v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mPrefs:Lde/robv/android/xposed/XSharedPreferences;
    invoke-static {v2}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$400(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lde/robv/android/xposed/XSharedPreferences;

    move-result-object v2

    const-string v3, "dynamic_colors"

    invoke-virtual {v2, v3, v4}, Lde/robv/android/xposed/XSharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->updateColors(Z)V

    .line 258
    .end local v0    # "level":I
    :cond_0
    :goto_1
    return-void

    .line 233
    .restart local v0    # "level":I
    :cond_1
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setBatteryCharging(Z)V

    goto :goto_0

    .line 239
    .end local v0    # "level":I
    :cond_2
    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 240
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 241
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setBatteryCharging(Z)V

    goto :goto_1

    .line 242
    :cond_3
    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 243
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 244
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setBatteryCharging(Z)V

    goto :goto_1

    .line 246
    :cond_4
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 247
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 248
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setScreenOn(Z)V

    goto :goto_1

    .line 249
    :cond_5
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 250
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 251
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setScreenOn(Z)V

    goto/16 :goto_1

    .line 253
    :cond_6
    const-string v1, "com.XtraordinairDEV.batteryhomebutton.SETTINGS_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 254
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # getter for: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->mBatteryDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$100(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 255
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod$6;->this$0:Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;

    # invokes: Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->reloadSettings()V
    invoke-static {v1}, Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;->access$500(Lcom/XtraordinairDEV/batteryhomebutton/XposedMod;)V

    goto/16 :goto_1
.end method
