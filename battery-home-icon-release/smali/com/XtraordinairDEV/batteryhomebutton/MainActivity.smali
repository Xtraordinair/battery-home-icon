.class public Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"


# instance fields
.field private mDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

.field private receiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 24
    new-instance v0, Lcom/XtraordinairDEV/batteryhomebutton/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/XtraordinairDEV/batteryhomebutton/MainActivity$1;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;)V

    iput-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;->receiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;)Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    .locals 1
    .param p0, "x0"    # Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;->mDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v2, 0x7f0d0050

    .line 18
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    const v0, 0x7f030019

    invoke-virtual {p0, v0}, Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;->setContentView(I)V

    .line 20
    new-instance v1, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, v0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;-><init>(Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;->mDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    .line 21
    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;->mDrawable:Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 22
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 43
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 44
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 36
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 37
    .local v0, "iF":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/XtraordinairDEV/batteryhomebutton/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 38
    return-void
.end method
