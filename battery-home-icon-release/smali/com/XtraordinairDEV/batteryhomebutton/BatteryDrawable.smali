.class public Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "BatteryDrawable.java"


# instance fields
.field private batteryChargingColor:I

.field private batteryDefaultColor:I

.field private batteryLowColor:I

.field private mAngle:I

.field private mAnimator:Landroid/animation/TimeAnimator;

.field private mCharging:Z

.field private mChargingAnimationEnabled:Z

.field private mEnablePercentage:Z

.field private mFontSize:I

.field private mFontSizePx:F

.field private mFullFontSizePx:F

.field private mLandscapePadding:I

.field private mLevel:I

.field private mPadding:I

.field private mPaint:Landroid/graphics/Paint;

.field private mRectF:Landroid/graphics/RectF;

.field private mRes:Landroid/content/res/Resources;

.field private mScreenOn:Z

.field private mTextPaint:Landroid/graphics/Paint;

.field private mView:Landroid/widget/ImageView;

.field private mWidth:I

.field private mZeroAngle:I


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;)V
    .locals 5
    .param p1, "view"    # Landroid/widget/ImageView;

    .prologue
    const/16 v4, 0x14

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, -0x1

    .line 56
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 31
    iput v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLevel:I

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAngle:I

    .line 33
    iput v4, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPadding:I

    .line 34
    iput v4, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLandscapePadding:I

    .line 35
    const/4 v0, 0x5

    iput v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mWidth:I

    .line 36
    const/16 v0, -0x5a

    iput v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mZeroAngle:I

    .line 40
    iput-boolean v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mChargingAnimationEnabled:Z

    .line 41
    iput-boolean v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mScreenOn:Z

    .line 47
    const/16 v0, 0xe

    iput v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mFontSize:I

    .line 57
    iput-object p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mView:Landroid/widget/ImageView;

    .line 58
    invoke-virtual {p1}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mRes:Landroid/content/res/Resources;

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPaint:Landroid/graphics/Paint;

    .line 61
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 62
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 63
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 65
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mTextPaint:Landroid/graphics/Paint;

    .line 66
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mTextPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 67
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 68
    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mFontSize:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mRes:Landroid/content/res/Resources;

    .line 69
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 68
    invoke-static {v3, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mFontSizePx:F

    .line 70
    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mFontSize:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mFontSize:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mRes:Landroid/content/res/Resources;

    .line 71
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 70
    invoke-static {v3, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mFullFontSizePx:F

    .line 72
    return-void
.end method

.method static synthetic access$002(Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;I)I
    .locals 0
    .param p0, "x0"    # Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAngle:I

    return p1
.end method

.method static synthetic access$100(Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;)I
    .locals 1
    .param p0, "x0"    # Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    .prologue
    .line 25
    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mZeroAngle:I

    return v0
.end method

.method static synthetic access$102(Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;I)I
    .locals 0
    .param p0, "x0"    # Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mZeroAngle:I

    return p1
.end method

.method static synthetic access$108(Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;)I
    .locals 2
    .param p0, "x0"    # Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    .prologue
    .line 25
    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mZeroAngle:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mZeroAngle:I

    return v0
.end method

.method static synthetic access$200(Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;)V
    .locals 0
    .param p0, "x0"    # Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->invalidate()V

    return-void
.end method

.method static synthetic access$300(Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;)Landroid/animation/TimeAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAnimator:Landroid/animation/TimeAnimator;

    return-object v0
.end method

.method private invalidate()V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->postInvalidate()V

    .line 287
    :goto_0
    return-void

    .line 286
    :cond_0
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->invalidateSelf()V

    goto :goto_0
.end method

.method private updateRectFromBounds(Landroid/graphics/Rect;)V
    .locals 11
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .prologue
    .line 98
    iget v6, p1, Landroid/graphics/Rect;->right:I

    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    if-le v6, v7, :cond_0

    .line 99
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    .line 100
    .local v5, "width":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    move-result v6

    div-int/lit8 v7, v5, 0x2

    sub-int v1, v6, v7

    .line 101
    .local v1, "left":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    move-result v6

    div-int/lit8 v7, v5, 0x2

    add-int v3, v6, v7

    .line 102
    .local v3, "right":I
    new-instance v2, Landroid/graphics/RectF;

    iget v6, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPadding:I

    add-int/2addr v6, v1

    int-to-float v6, v6

    iget v7, p1, Landroid/graphics/Rect;->top:I

    iget v8, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPadding:I

    add-int/2addr v7, v8

    int-to-float v7, v7

    iget v8, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPadding:I

    sub-int v8, v3, v8

    int-to-float v8, v8

    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    iget v10, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPadding:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    invoke-direct {v2, v6, v7, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 114
    .end local v1    # "left":I
    .end local v3    # "right":I
    .local v2, "rect":Landroid/graphics/RectF;
    :goto_0
    iput-object v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mRectF:Landroid/graphics/RectF;

    .line 115
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->invalidateSelf()V

    .line 116
    return-void

    .line 106
    .end local v2    # "rect":Landroid/graphics/RectF;
    .end local v5    # "width":I
    :cond_0
    iget v5, p1, Landroid/graphics/Rect;->right:I

    .line 107
    .restart local v5    # "width":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    move-result v6

    div-int/lit8 v7, v5, 0x2

    sub-int v4, v6, v7

    .line 108
    .local v4, "top":I
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    move-result v6

    div-int/lit8 v7, v5, 0x2

    add-int v0, v6, v7

    .line 109
    .local v0, "bottom":I
    new-instance v2, Landroid/graphics/RectF;

    iget v6, p1, Landroid/graphics/Rect;->left:I

    iget v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLandscapePadding:I

    add-int/2addr v6, v7

    int-to-float v6, v6

    iget v7, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLandscapePadding:I

    add-int/2addr v7, v4

    int-to-float v7, v7

    iget v8, p1, Landroid/graphics/Rect;->right:I

    iget v9, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLandscapePadding:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    iget v9, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLandscapePadding:I

    sub-int v9, v0, v9

    int-to-float v9, v9

    invoke-direct {v2, v6, v7, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .restart local v2    # "rect":Landroid/graphics/RectF;
    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 120
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mRectF:Landroid/graphics/RectF;

    if-nez v0, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v6

    .line 124
    .local v6, "alpha":I
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPaint:Landroid/graphics/Paint;

    const/16 v1, 0x46

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 127
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mRectF:Landroid/graphics/RectF;

    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAngle:I

    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mZeroAngle:I

    add-int/2addr v0, v2

    int-to-float v2, v0

    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAngle:I

    rsub-int v0, v0, 0x168

    int-to-float v3, v0

    iget-object v5, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 129
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 130
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mRectF:Landroid/graphics/RectF;

    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mZeroAngle:I

    int-to-float v2, v0

    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAngle:I

    int-to-float v3, v0

    iget-object v5, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 132
    iget-boolean v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mEnablePercentage:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLevel:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 133
    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLevel:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_2

    .line 134
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mTextPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mFullFontSizePx:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 138
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    div-int/lit8 v7, v0, 0x2

    .line 139
    .local v7, "xPos":I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->descent()F

    move-result v1

    iget-object v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->ascent()F

    move-result v2

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v8, v0

    .line 140
    .local v8, "yPos":I
    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLevel:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    int-to-float v1, v7

    int-to-float v2, v8

    iget-object v3, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 136
    .end local v7    # "xPos":I
    .end local v8    # "yPos":I
    :cond_2
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mTextPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mFontSizePx:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    goto :goto_1
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 93
    invoke-direct {p0, p1}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->updateRectFromBounds(Landroid/graphics/Rect;)V

    .line 94
    return-void
.end method

.method public setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 205
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 206
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 207
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->invalidateSelf()V

    .line 208
    return-void
.end method

.method public setBatteryCharging(Z)V
    .locals 4
    .param p1, "charging"    # Z

    .prologue
    .line 145
    iget-boolean v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mCharging:Z

    if-ne v1, p1, :cond_1

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    iput-boolean p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mCharging:Z

    .line 150
    iget-boolean v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mChargingAnimationEnabled:Z

    if-nez v1, :cond_2

    if-nez p1, :cond_0

    .line 153
    :cond_2
    iget-boolean v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mCharging:Z

    if-eqz v1, :cond_3

    .line 154
    new-instance v1, Landroid/animation/TimeAnimator;

    invoke-direct {v1}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAnimator:Landroid/animation/TimeAnimator;

    .line 155
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAnimator:Landroid/animation/TimeAnimator;

    new-instance v2, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable$2;

    invoke-direct {v2, p0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable$2;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;)V

    invoke-virtual {v1, v2}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    .line 166
    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v1}, Landroid/animation/TimeAnimator;->start()V

    goto :goto_0

    .line 168
    :cond_3
    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    iget v3, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mZeroAngle:I

    aput v3, v1, v2

    const/4 v2, 0x1

    const/16 v3, -0x5a

    aput v3, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 169
    .local v0, "animator":Landroid/animation/ValueAnimator;
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 170
    new-instance v1, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable$3;

    invoke-direct {v1, p0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable$3;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 186
    new-instance v1, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable$4;

    invoke-direct {v1, p0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable$4;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 193
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 194
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method public setBatteryChargingColor(I)V
    .locals 0
    .param p1, "batteryChargingColor"    # I

    .prologue
    .line 347
    iput p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryChargingColor:I

    .line 348
    return-void
.end method

.method public setBatteryDefaultColor(I)V
    .locals 0
    .param p1, "batteryDefaultColor"    # I

    .prologue
    .line 355
    iput p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryDefaultColor:I

    .line 356
    return-void
.end method

.method public setBatteryLevel(I)V
    .locals 4
    .param p1, "level"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLevel:I

    .line 77
    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    iget v3, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAngle:I

    aput v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLevel:I

    mul-int/lit16 v3, v3, 0x168

    div-int/lit8 v3, v3, 0x64

    aput v3, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 78
    .local v0, "animator":Landroid/animation/ValueAnimator;
    new-instance v1, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable$1;

    invoke-direct {v1, p0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable$1;-><init>(Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 85
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 86
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 87
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 88
    return-void
.end method

.method public setBatteryLowColor(I)V
    .locals 0
    .param p1, "batteryLowColor"    # I

    .prologue
    .line 351
    iput p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryLowColor:I

    .line 352
    return-void
.end method

.method public setChargingAnimationEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mChargingAnimationEnabled:Z

    .line 232
    .local v0, "oldChargingEnabled":Z
    iput-boolean p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mChargingAnimationEnabled:Z

    .line 234
    iget-boolean v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mCharging:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    iget-boolean v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mChargingAnimationEnabled:Z

    if-eqz v1, :cond_0

    .line 235
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mCharging:Z

    .line 236
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setBatteryCharging(Z)V

    .line 238
    :cond_0
    return-void
.end method

.method public setCircleColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 303
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 304
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 213
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 214
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->invalidateSelf()V

    .line 215
    return-void
.end method

.method public setPadding(II)V
    .locals 1
    .param p1, "padding"    # I
    .param p2, "landscapePadding"    # I

    .prologue
    .line 218
    iput p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPadding:I

    .line 219
    iput p2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLandscapePadding:I

    .line 220
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->updateRectFromBounds(Landroid/graphics/Rect;)V

    .line 221
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->invalidateSelf()V

    .line 222
    return-void
.end method

.method public setPercentageEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 241
    iget-boolean v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mEnablePercentage:Z

    if-ne v0, p1, :cond_0

    .line 246
    :goto_0
    return-void

    .line 244
    :cond_0
    iput-boolean p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mEnablePercentage:Z

    .line 245
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->invalidateSelf()V

    goto :goto_0
.end method

.method public setPercentageFontSize(I)V
    .locals 3
    .param p1, "size"    # I

    .prologue
    const/4 v2, 0x2

    .line 290
    iget v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mFontSize:I

    if-ne v0, p1, :cond_0

    .line 300
    :goto_0
    return-void

    .line 293
    :cond_0
    iput p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mFontSize:I

    .line 295
    int-to-float v0, p1

    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mRes:Landroid/content/res/Resources;

    .line 296
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 295
    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mFontSizePx:F

    .line 297
    add-int/lit8 v0, p1, -0x1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mRes:Landroid/content/res/Resources;

    .line 298
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 297
    invoke-static {v2, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mFullFontSizePx:F

    .line 299
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->invalidateSelf()V

    goto :goto_0
.end method

.method public setScreenOn(Z)V
    .locals 2
    .param p1, "screenOn"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 250
    iget-boolean v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mScreenOn:Z

    if-ne v0, p1, :cond_1

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    iput-boolean p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mScreenOn:Z

    .line 255
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAnimator:Landroid/animation/TimeAnimator;

    if-eqz v0, :cond_0

    .line 256
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_3

    .line 257
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    .line 258
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->pause()V

    .line 261
    :cond_2
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mCharging:Z

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->resume()V

    goto :goto_0

    .line 265
    :cond_3
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_4

    if-nez p1, :cond_4

    .line 266
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->end()V

    .line 269
    :cond_4
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mCharging:Z

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->start()V

    goto :goto_0
.end method

.method public setStrokeWidth(I)V
    .locals 2
    .param p1, "width"    # I

    .prologue
    .line 225
    iput p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mWidth:I

    .line 226
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 227
    invoke-virtual {p0}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->invalidateSelf()V

    .line 228
    return-void
.end method

.method public setTextColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 307
    iget-object v0, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 308
    return-void
.end method

.method public setView(Landroid/widget/ImageView;)V
    .locals 0
    .param p1, "view"    # Landroid/widget/ImageView;

    .prologue
    .line 277
    iput-object p1, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mView:Landroid/widget/ImageView;

    .line 278
    if-eqz p1, :cond_0

    .line 279
    invoke-virtual {p1}, Landroid/widget/ImageView;->postInvalidate()V

    .line 280
    :cond_0
    return-void
.end method

.method public updateColors(Z)V
    .locals 3
    .param p1, "colorIsDynamic"    # Z

    .prologue
    .line 312
    const/16 v1, 0xf

    .line 313
    .local v1, "BATTERY_LOW_THRESHOLD":I
    const/16 v0, 0x64

    .line 315
    .local v0, "BATTERY_FULL":I
    if-eqz p1, :cond_5

    .line 317
    iget-boolean v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mCharging:Z

    if-nez v2, :cond_2

    .line 318
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLevel:I

    if-gt v2, v1, :cond_1

    .line 319
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryLowColor:I

    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setTextColor(I)V

    .line 320
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryLowColor:I

    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setCircleColor(I)V

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryDefaultColor:I

    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setTextColor(I)V

    .line 323
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryDefaultColor:I

    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setCircleColor(I)V

    goto :goto_0

    .line 326
    :cond_2
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLevel:I

    if-gt v2, v1, :cond_3

    .line 327
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryLowColor:I

    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setTextColor(I)V

    .line 328
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryLowColor:I

    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setCircleColor(I)V

    goto :goto_0

    .line 329
    :cond_3
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLevel:I

    if-le v2, v1, :cond_4

    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLevel:I

    if-ge v2, v0, :cond_4

    .line 330
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryChargingColor:I

    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setTextColor(I)V

    .line 331
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryChargingColor:I

    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setCircleColor(I)V

    goto :goto_0

    .line 332
    :cond_4
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->mLevel:I

    if-ne v2, v0, :cond_0

    .line 333
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryDefaultColor:I

    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setTextColor(I)V

    .line 334
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryDefaultColor:I

    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setCircleColor(I)V

    .line 336
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setBatteryCharging(Z)V

    goto :goto_0

    .line 341
    :cond_5
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryDefaultColor:I

    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setTextColor(I)V

    .line 342
    iget v2, p0, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->batteryDefaultColor:I

    invoke-virtual {p0, v2}, Lcom/XtraordinairDEV/batteryhomebutton/BatteryDrawable;->setCircleColor(I)V

    goto :goto_0
.end method
