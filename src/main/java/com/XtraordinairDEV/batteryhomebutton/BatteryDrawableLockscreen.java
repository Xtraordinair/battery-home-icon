package com.XtraordinairDEV.batteryhomebutton;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.TimeAnimator;
import android.animation.TimeAnimator.TimeListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.TargetApi;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.TypedValue;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import de.robv.android.xposed.XSharedPreferences;

public class BatteryDrawableLockscreen extends Drawable implements BatteryGauge {
    public static final boolean SHOW_PERCENTAGE = true;
    public static final boolean HIDE_PERCENTAGE = false;
    private Paint mPaint;
    private Paint mTextPaint;

    private Resources mRes;

    private int mLevel = -1;
    private int mAngle = 0;
    private int mPadding = 20;
    private int mLandscapePadding = 20;
    private int mWidth = 5;
    private int mZeroAngle = -90; // The starting angle, angle 0 is actually 90 degrees on a circle
    private RectF mRectF;

    private boolean mCharging;
    private boolean mChargingAnimationEnabled = true;
    private boolean mScreenOn = true;
    private boolean mEnablePercentage;
    private boolean mShowText = true;

    private TimeAnimator mAnimator;

    private ImageView mView;
    private int mFontSize = 14;
    private float mFontSizePx;
    private float mFullFontSizePx;

    private int batteryChargingColor;
    private int batteryLowColor;
    private int batteryDefaultColor;

    public BatteryDrawableLockscreen(ImageView view) {
        super();
        mView = view;
        mRes = view.getResources();

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.WHITE);
        mPaint.setStrokeWidth(mWidth);

        mTextPaint = new Paint();
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        mTextPaint.setColor(Color.WHITE);
        mFontSizePx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, mFontSize,
                mRes.getDisplayMetrics());
        mFullFontSizePx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, mFontSize - 2,
                mRes.getDisplayMetrics());
    }

    public BatteryDrawableLockscreen(ImageView view, boolean showText) {
        super();
        mView = view;
        mRes = view.getResources();

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.WHITE);
        mPaint.setStrokeWidth(mWidth);

        mTextPaint = new Paint();
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        mTextPaint.setColor(Color.WHITE);
        mFontSizePx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, mFontSize,
                mRes.getDisplayMetrics());
        mFullFontSizePx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, mFontSize - 2,
                mRes.getDisplayMetrics());
        mShowText = showText;
    }

    public void setBatteryLevel(int level) {
        if(mLevel == level)
            return;

        mLevel = level;

        ValueAnimator animator = ValueAnimator.ofInt(mAngle, (mLevel * 360) / 100);
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mAngle = (Integer) animation.getAnimatedValue();
                invalidate();
            }
        });
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setDuration(1000);
        animator.start();
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        updateRectFromBounds(bounds);
    }

    private void updateRectFromBounds(Rect bounds) {
        RectF rect;
        if (bounds.right > bounds.bottom) {
            int width = bounds.bottom;
            int left = bounds.centerX() - (width / 2);
            int right = bounds.centerX() + (width / 2);
            rect = new RectF(left + mPadding,
                    bounds.top + mPadding, right - mPadding,
                    bounds.bottom - mPadding);
        } else {
            int width = bounds.right;
            int top = bounds.centerY() - (width / 2);
            int bottom = bounds.centerY() + (width / 2);
            rect = new RectF(bounds.left + mLandscapePadding,
                    top + mLandscapePadding, bounds.right - mLandscapePadding,
                    bottom - mLandscapePadding);
        }

        mRectF = rect;
        invalidateSelf();
    }

    @Override
    public void draw(Canvas canvas) {
        if (mRectF == null)
            return;
        //XposedBridge.log("BHI - Draw Called");
        int alpha = mPaint.getAlpha();

        mPaint.setAlpha(30);
        // We could use 0 - 360 here, but why overdraw?
        canvas.drawArc(mRectF, mAngle + mZeroAngle, 360 - mAngle, false, mPaint);

        mPaint.setAlpha(alpha);
        canvas.drawArc(mRectF, mZeroAngle, mAngle, false, mPaint);

        if (mEnablePercentage && mLevel != -1) {
            if (mLevel == 100) {
                mTextPaint.setTextSize(mFullFontSizePx);
            } else {
                mTextPaint.setTextSize(mFontSizePx);
            }
            int xPos = (canvas.getWidth() / 2);
            int yPos = (int) ((canvas.getHeight() / 2) - ((mTextPaint.descent() + mTextPaint.ascent()) / 2));
            canvas.drawText(String.valueOf(mLevel), xPos, yPos, mTextPaint);
        }
    }

    public void setBatteryCharging(boolean charging) {
        if (mCharging == charging)
            return;

        mCharging = charging;

        if (!mChargingAnimationEnabled && charging)
            return;

        if (mCharging) {
            mAnimator = new TimeAnimator();
            mAnimator.setTimeListener(new TimeListener() {
                @Override
                public void onTimeUpdate(TimeAnimator arg0, long arg1, long arg2) {
                    if (mZeroAngle == 270)
                        mZeroAngle = -90;
                    else
                        mZeroAngle++;

                    invalidate();
                }
            });
            mAnimator.start();
        } else {
            ValueAnimator animator = ValueAnimator.ofInt(mZeroAngle, -90);
            animator.setDuration(1000L);
            animator.addListener(new AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    if (mAnimator != null)
                        mAnimator.cancel();
                }

                @Override
                public void onAnimationRepeat(Animator animation) { }

                @Override
                public void onAnimationEnd(Animator animation) { }

                @Override
                public void onAnimationCancel(Animator animation) { }
            });
            animator.addUpdateListener(new AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    mZeroAngle = (Integer) animator.getAnimatedValue();
                    invalidate();
                }
            });
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.start();
        }
    }

    @Override
    public int getOpacity() {
        return 0;
    }

    @Override
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
        mTextPaint.setAlpha(alpha);
        invalidateSelf();
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        mPaint.setColorFilter(cf);
        mTextPaint.setColorFilter(cf);
        invalidateSelf();
    }

    public void setPadding(int padding, int landscapePadding) {
        mPadding = padding;
        mLandscapePadding = landscapePadding;
        updateRectFromBounds(getBounds());
        invalidateSelf();
    }

    public void setStrokeWidth(int width) {
        mWidth = width;
        mPaint.setStrokeWidth(mWidth);
        invalidateSelf();
    }

    public void setChargingAnimationEnabled(boolean enable) {
        boolean oldChargingEnabled = mChargingAnimationEnabled;
        mChargingAnimationEnabled = enable;

        if (mCharging && !oldChargingEnabled && mChargingAnimationEnabled) {
            mCharging = false;
            setBatteryCharging(true);
        }
    }

    public void setPercentageEnabled(boolean enable) {
        if(!mShowText)
            return;

        if (mEnablePercentage == enable)
            return;

        mEnablePercentage = enable;
        invalidateSelf();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void setScreenOn(boolean screenOn) {
        if (mScreenOn == screenOn)
            return;

        mScreenOn = screenOn;

        if (mAnimator != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (mAnimator.isRunning() && !screenOn) {
                    mAnimator.pause();
                }

                if (mAnimator.isPaused() && screenOn && mCharging) {
                    mAnimator.resume();
                }
            } else {
                if (mAnimator.isRunning() && !screenOn) {
                    mAnimator.end();
                }

                if (!mAnimator.isRunning() && screenOn && mCharging) {
                    mAnimator.start();
                }
            }
        }
    }

    public void setView(ImageView view) {
        mView = view;
        if (view != null)
            view.postInvalidate();
    }

    private void invalidate() {
        if (mView != null)
            mView.postInvalidate();
        else
            invalidateSelf();
    }

    public void setPercentageFontSize(int size) {
        if (mFontSize == size)
            return;

        mFontSize = size;

        mFontSizePx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, size,
                mRes.getDisplayMetrics());
        mFullFontSizePx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, size - 2,
                mRes.getDisplayMetrics());
        invalidateSelf();
    }

    public void setCircleColor(int color){
        if(mPaint.getColor() == color)
            return;

        mPaint.setColor(color);
    }

    public void setTextColor(int color){
        if(mTextPaint.getColor() == color)
            return;

        mTextPaint.setColor(color);
    }

    public void updateColors(boolean colorIsDynamic) {

        int BATTERY_LOW_THRESHOLD = 15;
        int BATTERY_FULL = 100;

        if (colorIsDynamic){
            //Check if phone is charging
            if (!(mCharging)) {
                if (mLevel <= BATTERY_LOW_THRESHOLD) {
                    this.setTextColor(batteryLowColor);
                    this.setCircleColor(batteryLowColor);
                } else {
                    this.setTextColor(batteryDefaultColor);
                    this.setCircleColor(batteryDefaultColor);
                }
            } else {
                if (mLevel <= BATTERY_LOW_THRESHOLD) {
                    this.setTextColor(batteryLowColor);
                    this.setCircleColor(batteryLowColor);
                } else if (mLevel > BATTERY_LOW_THRESHOLD && mLevel < BATTERY_FULL) {
                    this.setTextColor(batteryChargingColor);
                    this.setCircleColor(batteryChargingColor);
                } else if (mLevel == BATTERY_FULL) {
                    this.setTextColor(batteryDefaultColor);
                    this.setCircleColor(batteryDefaultColor);
                    //Stop spinning circle, users can't tell it's spinning
                    this.setBatteryCharging(false);
                }

            }
        }else{
            this.setTextColor(batteryDefaultColor);
            this.setCircleColor(batteryDefaultColor);
        }
    }

    public void setBatteryChargingColor(int batteryChargingColor) {
        this.batteryChargingColor = batteryChargingColor;
    }

    public void setBatteryLowColor(int batteryLowColor) {
        this.batteryLowColor = batteryLowColor;
    }

    public void setBatteryDefaultColor(int batteryDefaultColor) {
        this.batteryDefaultColor = batteryDefaultColor;
    }

    public void reloadSettings() {
        XSharedPreferences mPrefs
                = new XSharedPreferences("com.XtraordinairDEV.batteryhomebutton");

        mPrefs.reload();
        int padding = (int) (mPrefs.getFloat("padding_portrait_lockscreen", 0.7F) * 50);
        int lPadding = (int) (mPrefs.getFloat("padding_landscape_lockscreen", 0.5F) * 50);
        int width = (int) (mPrefs.getFloat("stroke_width_lockscreen", 0.1F) * 50);
        int batteryChargingColor = mPrefs.getInt("battery_charging_color", 0xFF2EB82E);
        int batteryLowColor = mPrefs.getInt("battery_low_color", 0xFF660000);
        int batteryDefaultColor = mPrefs.getInt("battery_default_color", 0xFFFFFFFF);
        boolean colorIsDynamic = mPrefs.getBoolean("dynamic_colors", false);
        this.setChargingAnimationEnabled(mPrefs.getBoolean("charging_animation", true));
        this.setPercentageFontSize(Integer.parseInt(mPrefs.getString("battery_percentage_size", "14")));
        this.setPercentageEnabled(mPrefs.getBoolean("battery_percentage", true));
        this.setPadding(padding, lPadding);
        this.setStrokeWidth(width);
        this.setBatteryDefaultColor(batteryDefaultColor);
        this.setBatteryLowColor(batteryLowColor);
        this.setBatteryChargingColor(batteryChargingColor);
        this.updateColors(colorIsDynamic);
    }

    public static BatteryDrawableLockscreen newInstance(ImageView imageView, boolean showText){
        BatteryDrawableLockscreen batteryDrawable = new BatteryDrawableLockscreen(imageView, showText);
        batteryDrawable.reloadSettings();
        return batteryDrawable;
    }
}
