package com.XtraordinairDEV.batteryhomebutton;

import android.os.Build;
import android.view.View;

import java.util.Locale;

import de.robv.android.xposed.IXposedHookInitPackageResources;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.callbacks.XC_InitPackageResources;
import de.robv.android.xposed.callbacks.XC_LayoutInflated;

/**
 * Created by Steph on 6/12/2016.
 */
public class XposedBatteryHide implements IXposedHookInitPackageResources {
    private final String SYSTEMUI = "com.android.systemui";
    private XSharedPreferences mPrefs
            = new XSharedPreferences("com.XtraordinairDEV.batteryhomebutton");

    @Override
    public void handleInitPackageResources(XC_InitPackageResources.InitPackageResourcesParam resparam) throws Throwable {
        if (!SYSTEMUI.equals(resparam.packageName))
            return;

        boolean hideBattery = mPrefs.getBoolean("hide_battery", false);
        if(hideBattery) {
            int resId = 0;
            boolean isSonyDevice = Build.MANUFACTURER.toLowerCase(Locale.getDefault()).equals("sony");
            if (isSonyDevice) {
                resId = resparam.res.getIdentifier("msim_super_status_bar", "layout", SYSTEMUI);
            }
            if (resId == 0) {
                resId = resparam.res.getIdentifier("super_status_bar", "layout", SYSTEMUI);
            }
            try {
                resparam.res.hookLayout(resId, new XC_LayoutInflated() {
                    @Override
                    public void handleLayoutInflated(LayoutInflatedParam liparam) throws Throwable {
                        View view = liparam.view.findViewById(liparam.res.getIdentifier("battery",
                                "id", SYSTEMUI));

                        if (view != null)
                            view.setVisibility(View.GONE);
                    }
                });
            } catch (Exception e) {
                XposedBridge.log("Failed to hide battery: " + e.getMessage());
            }
        }
    }
}
