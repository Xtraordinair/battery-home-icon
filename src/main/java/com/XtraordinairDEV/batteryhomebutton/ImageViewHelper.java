package com.XtraordinairDEV.batteryhomebutton;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.view.View;
import android.widget.ImageView;

import de.robv.android.xposed.XSharedPreferences;

/**
 * Created by Steph on 6/11/2016.
 */
public class ImageViewHelper {
    private final BatteryGauge mBatteryDrawable;
    private ImageView mImageView;
    private BroadcastReceiver mBroadcastReceiver;

    public ImageViewHelper(ImageView imageView, BatteryGauge batteryDrawable){
        mImageView = imageView;
        mBatteryDrawable = batteryDrawable;
    }

    public void setReceiver(){
        final XSharedPreferences mPrefs
                = new XSharedPreferences("com.XtraordinairDEV.batteryhomebutton");

        IntentFilter iF = new IntentFilter();
        iF.addAction(Intent.ACTION_BATTERY_CHANGED);
        iF.addAction(Intent.ACTION_POWER_CONNECTED);
        iF.addAction(Intent.ACTION_POWER_DISCONNECTED);
        iF.addAction(Intent.ACTION_SCREEN_ON);
        iF.addAction(Intent.ACTION_SCREEN_OFF);
        iF.addAction(PreferencesActivity.INTENT_SETTINGS_CHANGED);

        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent intent) {

                //Something about battery changed (level, charging, etc)
                if (Intent.ACTION_BATTERY_CHANGED.equals(intent.getAction())) {

                    if (mBatteryDrawable != null) {
                        int level = (100 * intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0)
                                / intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100));
                        mBatteryDrawable.setBatteryLevel(level);

                        if (intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0) == 0) {
                            mBatteryDrawable.setBatteryCharging(false);
                        }else{
                            mBatteryDrawable.setBatteryCharging(true);
                        }

                        boolean dynamicColors = mPrefs.getBoolean("dynamic_colors", false);

                        if(dynamicColors) {
                            mBatteryDrawable.updateColors(mPrefs.getBoolean("dynamic_colors", false));
                        }
                    }
                } //Legacy check if phone is charging
                else if (Intent.ACTION_POWER_CONNECTED.equals(intent.getAction())) {
                    if (mBatteryDrawable != null)
                        mBatteryDrawable.setBatteryCharging(true);
                } else if (Intent.ACTION_POWER_DISCONNECTED.equals(intent.getAction())) {
                    if (mBatteryDrawable != null)
                        mBatteryDrawable.setBatteryCharging(false);
                } //Check if screen is on/off
                else if (Intent.ACTION_SCREEN_ON.equals(intent.getAction())) {
                    if (mBatteryDrawable != null)
                        mBatteryDrawable.setScreenOn(true);
                } else if (Intent.ACTION_SCREEN_OFF.equals(intent.getAction())) {
                    if (mBatteryDrawable != null)
                        mBatteryDrawable.setScreenOn(false);
                } //Update home icon on Preference Change
                else if (PreferencesActivity.INTENT_SETTINGS_CHANGED.equals(intent.getAction())) {
                    if (mBatteryDrawable != null) {
                        mBatteryDrawable.reloadSettings();
                    }
                }
            }
        };

        mBroadcastReceiver = broadcastReceiver;
        mImageView.getContext().registerReceiver(mBroadcastReceiver, iF);
    }

    public void onWindowEvent(){

        mImageView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
                setReceiver();
            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                mImageView.getContext().unregisterReceiver(mBroadcastReceiver);
            }
        });
    }
}
