package com.XtraordinairDEV.batteryhomebutton;

import android.widget.ImageView;

/**
 * Created by Steph on 8/23/2016.
 */
public interface BatteryGauge {
    void setScreenOn(boolean isOn);

    void setBatteryCharging(boolean isCharging);

    void reloadSettings();

    void setView(ImageView view);

    void setBatteryLevel(int level);

    void updateColors(boolean dynamicColors);
}
